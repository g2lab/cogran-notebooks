# cogran-notebooks

A set of introductory Jupyter notebooks for CoGran.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/g2lab%2Fcogran-notebooks/master)
